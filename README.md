Enshrouded key macros
===

1. [Features](#features)
1. [Demonstration](#demonstration)
1. [Installation steps](#installation-steps)


Features
--
- F -> repeated for looting.  
  **Add 'F' as secondary key binding for 'Contextual Action'** in Enshrouded in 'Settings' -> 'Controls' -> 'Keyboard & Mouse Bindings'. This is necessary to 'Loot All' automatically (hardwired to F) and to be able to 'Pick Up' or 'Dismantle' objects with 'E' (does not work if 'E' is repeated).
- Middle mouse button -> repeats Left mouse button for planting and continuous attacking.
- Space -> repeated for crafting, flying and hillclimbing.
- CapsLock -> reopen the glider (X on press + Space on release).  
  **Add 'X' as secondary key binding for 'Cancel'** in Enshrouded in 'Settings' -> 'Controls' -> 'Keyboard & Mouse Bindings'. Right mouse button is not suitable due to interfering with Dodge and aiming: it raises the shield/bow mid-air instead of gliding. This is why people remove the shield before flying.
- Ctrl-Enter -> cook (roast) a stack of raw food. Asks the size of the stack.  
  To abort cooking press Enter or the Left mouse button.
- Windows key -> disabled, redirected to Win-Insert.
- Win-Tab -> acts as Alt-Tab.

The hotkeys can be changed by editing the script.



Demonstration
--
| flying: | cooking: | planting: | harvesting: | crafting: |
| :-----: | :------: | :-------: | :---------: | :-------: |
| [![flying demonstration video](.assets/thumbnails/flying-demo-1.jpg)](https://www.youtube.com/watch?v=LFHEvbZ8s3s&list=PLFIMgrn29fNvLkX9sskwbVpnKdjXWQaYf&index=2) | [![cooking demonstration video](.assets/thumbnails/cooking-demo.jpg)](https://www.youtube.com/watch?v=Aopu2gqnMdc&list=PLFIMgrn29fNtz9VyNfRIj0yfSDGmpxLYY&index=4) | [![planting demonstration video](.assets/thumbnails/planting-demo.jpg)](https://www.youtube.com/watch?v=9QJBPN3T8TA&list=PLFIMgrn29fNtz9VyNfRIj0yfSDGmpxLYY&index=1) | [![harvesting demonstration video](.assets/thumbnails/harvesting-demo.jpg)](https://www.youtube.com/watch?v=oz5W5BlnE7o&list=PLFIMgrn29fNtz9VyNfRIj0yfSDGmpxLYY&index=2) | [![crafting demonstration video](.assets/thumbnails/crafting-demo.jpg)](https://www.youtube.com/watch?v=xU33UzhPSk8&list=PLFIMgrn29fNtz9VyNfRIj0yfSDGmpxLYY&index=3) |



Installation steps
--
1. Download and install [AutoHotKey v2.0](https://www.autohotkey.com/)
1. Download [the macros](https://gitlab.com/gobozen/enshrouded-key-macros/-/raw/main/Enshrouded-autohotkey.ahk)
1. Run `Enshrouded-autohotkey.ahk` as it is. The AutoHotKey icon (![AutoHotKey icon](.assets/ahk16.png)) will appear in the taskbar tray.
1. Switch to Enshrouded, no need to restart the game.
1. Add 'F' as secondary key binding for 'Contextual Action' in 'Settings' -> 'Controls' -> 'Keyboard & Mouse Bindings' for 'Contextual Action'.
1. Optional: adding a shortcut for Enshrouded-autohotkey.ahk to the Start menu's Startup folder will load it automatically.

The hotkey macros are only active in Enshrouded, won't affect other programs.

- To disable the hotkey macros: right-click the tray icon, click 'Exit' or 'Suspend Hotkeys'.
- To change the hotkeys: right-click the tray icon, click 'Edit Script', see the comments below 'Configuration'.

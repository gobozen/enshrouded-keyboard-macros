﻿;; Enshrouded: repeat keys for planting, harvesting, crafting, cooking, flying and hillclimbing
;; version: 2024-02-27
;; AutoHotKey v2.0 required:  https://www.autohotkey.com/

;; Only active in Enshrouded
scriptTitle := "Enshrouded key macros"
gameWindowTitle := "ahk_exe enshrouded.exe"
HotIfWinActive(gameWindowTitle)



;;;; Configuration

;; F: repeat for looting
;; Add 'F' as secondary key binding for 'Contextual Action' in Enshrouded in 'Settings' -> 'Controls' -> 'Keyboard & Mouse Bindings'.
;; Or: change "f" on the line for `macrokeyForLootingContinuously` to your preferred key for repetition.
;; The list of key names:  https://www.autohotkey.com/docs/v2/KeyList.htm
;; 'F' is hardwired to 'Loot All', therefore my preferred choice. Note: it's also hardwired to 'Pin', 'Show on Map', etc.
;; Note: to be able to 'Pick Up' or 'Dismantle' objects 'E' cannot be repeated.
macrokeyForLootingContinuously := "f"         ;; press this to spam looting
keyForLooting := "f"                          ;; set as secondary key binding in 'Keyboard & Mouse Bindings' for 'Contextual Action'

;; MiddleMouseButton: repeat LeftMouseButton for planting
macrokeyForUseItemContinuously := "MButton"   ;; press this to spam 'Use Item' (e.g. planting)
keyForUseItem := "LButton"                    ;; key binding for 'Use Item' in 'Keyboard & Mouse Bindings'

;; Space: repeat for crafting, flying and hillclimbing
macrokeyForCraftingContinuously := "Space"    ;; press this to spam 'Craft'
keyForCrafting := "Space"                     ;; hardwired to 'Craft'


;; CapsLock: reopen the glider (X on press + Space on release)
;; Add 'X' as secondary key binding for 'Cancel' in Enshrouded in 'Settings' -> 'Controls' -> 'Keyboard & Mouse Bindings'.
;; Or: change "x" on the line for `keyForCancel` to your preferred key for repetition.
;; Note: RightMouseButton ("RButton") is not suitable due to interfering with Dodge and aiming: it raises the shield/bow mid-air instead of gliding. This is why people remove the shield before flying. Changing the key binding is more practical.
macrokeyForReopenGliderOrFall := "CapsLock"
keyForCancel := "x"
keyForJump := "Space"
delayBetweenGliderOpens := 50                 ;; Delay between glider opening attempts: 50ms

;; CapsLock (alternative): reopen the glider (X + Space)
macrokeyForReopenGliderFast := ""             ;; "": disabled


;; Ctrl-Enter: cook (roast) a stack of raw food
keyForCookingContinuously := "^Enter"   ;; ^Enter means Ctrl-Enter, press this to repeatedly roast food on fire with proper timings
keyForStopCooking := "Enter"            ;; Press this or Space to stop cooking
delayWhileCooking := 5500               ;; 5.5 seconds: how long (milliseconds) to cook one food item
maxFoodStack := 20                      ;; Max items in a stack for cooking


;; Windows key: disabled, redirected to Win-Insert. Write comma (;) before the line if not needed
HotKey  "~LWin", (*) => Send("{Blind}{Insert}")

;; Win-Tab: acts as Alt-Tab
HotKey  "<#Tab", (*) => Send("!{Tab}")
;; alternatives
;HotKey  "<#Tab", (*) => Send("{Alt down}{Tab}{Alt up}")
;HotKey  "<#Tab", "AltTab"



;;;; Features

;; F: repeat for looting
;; * continuous looting (e.g. 'Harvest', bushes, critters)
;; - unable to do 'Pick Up' on objects, therefore another mapping is also needed for 'Contextual Action' (E by default)

;; MiddleMouseButton: repeat LeftMouseButton for planting
;; * continuous planting: walk in a row, use snapping
;; * continuous attacking
;; - unable to do drag-and-drop items in inventory, therefore it's not the LeftMouseButton remapped

;; Space: repeat for crafting, flying and hillclimbing
;; * crafting dozens of items
;; * continuous jumps on hillsides
;; * flying with 'Cancel' (default: RightMouseButton) pressed occasionally to allow repeated 'Updraft'


;; CapsLock: reopen the glider (X on press + Space on release)
;; * cancel and open glider again
;; * keep pressed to fall until it's released
;; - disables CapsLock (not needed ingame)

;; CapsLock (alternative): reopen the glider (X + Space)
;; * cancel and open glider again
;; * might be marginally faster than opening on release (the variant above)
;; - disables CapsLock (not needed ingame)


;; Ctrl-Enter: cook (roast) a stack of raw food
;; * pressing LeftMouseButton for 5.5 seconds
;; * asks how many times to cook (20 by default)
;; * stops if Enter/Space/LeftMouseButton is pressed
;; * if stack runs out before 20 repetitions then it will start hitting the fireplace



;;;; Delay settings

;; Delay between repetitions: 100ms
;; Less delay causes stutters, more is slow. The right setting may depend on your hardware.
delayBetweenRepetitions := 100

;; Delay more before repetition starts: 300ms
;; This is to avoid unintended repetition in the menus
delayBeforeRepetition := 300




;;;; Hotkey definitions

;; F: repeat for looting
Hotkey  macrokeyForLootingContinuously, (ThisHotkey) => RepeatWhilePressed(ThisHotkey, keyForLooting)

;; MiddleMouseButton: repeat LeftMouseButton for planting
Hotkey  macrokeyForUseItemContinuously, (ThisHotkey) => RepeatWhilePressed(ThisHotkey, keyForUseItem)

;; Space: repeat for crafting, flying and hillclimbing
Hotkey  macrokeyForCraftingContinuously, (ThisHotkey) => RepeatWhilePressed(ThisHotkey, keyForCrafting)


;; CapsLock: reopen the glider (X on press + Space on release)
if macrokeyForReopenGliderOrFall {
  Hotkey  macrokeyForReopenGliderOrFall, (ThisHotkey) => SendKeys(keyForCancel)
  Hotkey  macrokeyForReopenGliderOrFall " up", (ThisHotkey) => SendKeys(delayBetweenGliderOpens, keyForJump, keyForJump, keyForJump, keyForJump)
}

;; CapsLock (alternative): reopen the glider (X + Space)
if macrokeyForReopenGliderFast {
  Hotkey  macrokeyForReopenGliderFast, (ThisHotkey) => ReopenGlider(ThisHotkey, keyForCancel, keyForJump)
}


;; Ctrl-Enter: cook (roast) a stack of raw food
if keyForCookingContinuously {
  Hotkey  keyForCookingContinuously, (ThisHotkey) => RepeatOrStopCooking(ThisHotkey, keyForUseItem)
  ;; Pressing Enter stops the macro. ~ means the keypress is sent to the game.
  Hotkey  "~" keyForStopCooking, (ThisHotkey) => functionStopCooking(ThisHotkey)
  ;; Pressing LeftMouseButton stops the macro. ~ means the button press is sent to the game: keeping the button pressed continues cooking until released.
  Hotkey  "~" keyForUseItem, (ThisHotkey) => functionStopCooking(ThisHotkey)
}




;;;; Repeater function

RepeatWhilePressed(keyPressed, keyRepeated?) {
  keyRepeated := keyRepeated ?? keyPressed
  tosend := "{" keyRepeated "}"

  ;; This delay between keypresses is only effective for this hotkey
  SetKeyDelay(delayBetweenRepetitions, 0)

  SendEvent(tosend)
  Sleep(delayBeforeRepetition-delayBetweenRepetitions)  ;; Delay more before repetition starts

  while GetKeyState(keyPressed, "P") {              ;; "P" means physical state: is it pressed by the user
    SendEvent(tosend)
    ;; Delayed 100ms by SetKeyDelay
  }
}



;;;; Glider

;; Press 'X' then 'Space' 3 times, 50ms delay between keypresses
ReopenGlider(keyPressed, keyForCancel, keyForJump) {
  SendKeys(delayBetweenGliderOpens, keyForCancel, keyForJump, keyForJump, keyForJump)
  ; Prevent repetition of ReopenGlider while CapsLock is pressed. Repeatedly reopening the glider uses lots of stamina.
  while GetKeyState(keyPressed, "P") {              ;; "P" means physical state: is it pressed by the user
    Sleep(delayBetweenGliderOpens)
  }
}



;;;; Cooking

functionStopCooking := NoOp
functionCookNext := 0

;; Repeater function for cooking
RepeatOrStopCooking(keyPressed, keyRepeated) {
  keyRepeated := keyRepeated ?? keyPressed
  gameWindow := WinExist(gameWindowTitle)
  global functionStopCooking
  global functionCookNext

  if not gameWindow {
    MsgBox("Failed to find game window: '" gameWindowTitle "'", scriptTitle)
    return
  }

  stackLeft := maxFoodStack
  if functionCookNext {
    StopCooking(keyPressed)
  } else {
    input := InputBox("How many to cook?", scriptTitle, "w200 h100", maxFoodStack)
    if input.Result != "OK"
      return
    if not IsInteger(input.Value) {
      MsgBox("Aborting, not a number: '" input.Value "'", scriptTitle)
      return
    }
    stackLeft := Integer(input.Value)
    StartCooking()
  }

  StartCooking() {
    global functionStopCooking
    global functionCookNext
    functionStopCooking := StopCooking
    functionCookNext := CookNext
    SetTimer(functionCookNext, delayWhileCooking)
    ;; Start cooking: press LeftMouseButton
    SendRepeatedKeyToGame("down")
  }

  CookNext() {
    ;; Do at most maxFoodStack repetitions
    if (--stackLeft <= 0) {
      StopCooking()
      return
    }
    ;; Finish cooking and start with the next: release and press LeftMouseButton
    SendRepeatedKeyToGame("up")
    SendRepeatedKeyToGame("down")
  }

  StopCooking(keyPressed := "") {
    global functionStopCooking
    global functionCookNext
    if not functionCookNext
      return

    ;; Stop repeater
    SetTimer(functionCookNext, 0)
    functionCookNext := 0
    functionStopCooking := NoOp
    
    ;; If LeftMouseButton is not pressed by the user then release LeftMouseButton (abort cooking)
    if keyPressed != keyForUseItem or keyPressed and not GetKeyState(keyPressed, "P") {
      SendRepeatedKeyToGame("up")
    }
  }

  ;; Send a keypress or a button click to the game window
  SendRepeatedKeyToGame(upOrDown) {
    ControlSendOrClick(keyRepeated, upOrDown, , gameWindow)
  }
}

;;;; END Cooking



;;;; Utility functions

;; Send a list of keys (any number of parameters)
;; The first parameter can be a number in which case it is set to be the delay between keypresses in milliseconds
SendKeys(keys*) {
  if IsInteger(keys[1]) {
    ;; This delay between keypresses is only effective for this hotkey
    SetKeyDelay(keys[1], 0)
    keys.RemoveAt(1)  
  }
  for key in keys {
    SendEvent("{" key "}")
  }
}

;; Send a keypress or a button click to a control/window, like SendEvent does
ControlSendOrClick(key, upOrDown, Control?, WinTitle?, WinText?, ExcludeTitle?, ExcludeText?) {
  button := KeyToButton(key)
  if button {
    uOrD := SubStr(upOrDown, 1, 1)
    ControlClick(Control?, WinTitle?, WinText?, button, , uOrD, ExcludeTitle?, ExcludeText?)
  } else {
    ControlSend("{" key " " upOrDown "}", Control?, WinTitle?, WinText?, ExcludeTitle?, ExcludeText?)
  }
}

;; Convert key name to mouse button: LButton -> L, MButton -> M, RButton -> R, XButton1 -> X1, XButton2 -> X2
;; Return wheel events as-is: WheelUp/WheelDown/WheelLeft/WheelRight
KeyToButton(key) {
  if SubStr(key, 2, 6) == "Button" {
    return SubStr(key, 1, 1) SubStr(key, 8)  ;; return first character which is L/M/R/X + last character which is empty/1/2
  }
  if SubStr(key, 1, 5) == "Wheel" {
    return key  ;; return WheelUp/WheelDown/WheelLeft/WheelRight
  }
}

;; Empty function
NoOp(*) {
}

;;;; END Utility functions
